	<!DOCTYPE html>
    <html lang="ru">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
$title="Arcmovie Sports Nutrition: per atleti e amanti";
$description="Ogni allenatore di palestra e sport di potenza dirà che l'alimentazione sportiva è necessaria per ottenere risultati più rapidi in allenamento. Oggi è impossibile ottenere risultati elevati con le nostre riserve del corpo o con un'alimentazione scadente e di bassa qualità nutrizionale. ";

$arr_pages = array(array("index", 
	"Arcmovie Sports Nutrition: per atleti e amanti", 
	"Ogni allenatore di palestra e sport di potenza dirà che l'alimentazione sportiva è necessaria per ottenere risultati più rapidi in allenamento. Oggi è impossibile ottenere risultati elevati con le nostre riserve del corpo o con un'alimentazione scadente e di bassa qualità nutrizionale."),

array("blog_trav",
	"Suggerimenti per l'alimentazione sportiva",
	"Un tipico errore dei principianti è una completa transizione alla nutrizione sportiva. Raccomandiamo vivamente di non farlo! Il rifiuto dei prodotti convenzionali porterà a una carenza ancora maggiore di nutrienti, perché è nei prodotti ordinari che contengono la maggior parte di proteine, grassi e carboidrati. Gli atleti esperti raccomandano prima di assumere qualsiasi consulente"),

array("kak_pravilno",
	"Nutrizione sportiva, cosa prendere e come",
	"La nutrizione sportiva e gli integratori alimentari specializzati sono integratori biologicamente attivi e salutari, che includono esclusivamente ingredienti naturali. Gli specialisti distinguono gli integratori in diversi tipi, ognuno dei quali è progettato in modo che un atleta che li prende possa raggiungere determinati obiettivi sportivi."),

array("svyaz_s_nami",
    "Nutrizione sportiva: Directory del negozio",
    "Nel mondo degli integratori sportivi, le miscele proteiche in qualche modo hanno preso il posto di leader in tutte le altre categorie. Questo perché qualsiasi tipo di proteina può essere utilizzata per raggiungere gli obiettivi più diversi. In effetti, il fatto che la proteina svolga un ruolo importante nel nostro corpo e quanto dipende dal suo uso è davvero sorprendente."),

array("vse_o_travah",
    "Ordina Sports Nutrition su Arcmovie",
    "Per ulteriori informazioni, chiamaci o scrivi una lettera al nostro indirizzo e-mail"),
);
foreach ($arr_pages as &$page) {
    if(strpos($_SERVER["REQUEST_URI"],$page[0]) !== false){
    	$title=$page[1];
    	$description=$page[2];
    }
}
?>
    <title><?=$title?></title>
    <meta name="description" content="<?=$description?>">

        <link rel="shortcut icon" href="/include/img/favicon.ico">

<style>body{font-family:'Open Sans',sans-serif;overflow-x:hidden}h1,h2{font-family:'Montserrat',sans-serif}a,button{text-decoration:none!important;outline:none!important;box-shadow:none!important}.section{position:relative;padding-top:80px;padding-bottom:80px}.sec_subtitle{max-width:550px;line-height:1.6}.vr_line{height:30px;width:3px;background-color:#702}.btn{padding:12px 26px}.btn-rounded{border-radius:30px}.btn-outline-custom{background-color:#fffefe3d;border:2px solid #702;color:#702;letter-spacing:1px;font-size:15px;margin-bottom:20px}.btn-outline-custom{background-color:#fffefe96;border:2px solid #702;color:#702;letter-spacing:1px;font-size:15px;margin-bottom:20px}.bg-overlay{background:rgba(0,0,0,.7);position:absolute;top:0;right:0;left:0;bottom:0;width:100%;height:100%}.custom-nav{padding:22px 0;width:100%;border-radius:0;z-index:999;margin-bottom:0;background-color:#8aa730;height: 55px;}.custom-nav .navbar-nav li a{color:#fff;font-size:14px;background-color:transparent!important;padding:7px 0!important;position:relative;margin:0 18px;border-width:5px}.custom-nav .navbar-nav li a:after{content:'';width:20px;height:2px;left:0;bottom:0;position:absolute;opacity:0;background:#702}.navbar-toggler{font-size:24px;margin-top:5px;margin-bottom:0;color:#fff}.logo .logo-light{display:inline-block}.logo .logo-dark{display:none}.custom-nav .navbar-brand.logo img{height:50px}.home-bg1{background-image:url(/include/img/sld1.jpg);background-position:center center;background-size:cover;background-attachment:fixed;position:relative}.home-table-center{display:table-cell;vertical-align:middle}.home-table{display:table;width:100%;height:100%}.header_title{font-size:52px;font-weight:700;line-height:1.2}.header_subtitle{line-height:2;font-size:18px;max-width:600px}.home-clip{clip-path:polygon(0 0,100% 0,100% 83%,50% 100%,0 83%)}.client_boxes{min-height:345px;background-color:#fff;border:1px solid;border-radius:10px;border-color:#8887873d}.rounded-circle{border-radius:50%!important}@media (max-width:768px){.custom-nav{margin-top:0;padding:10px 0!important;background-color:#8aa730!important}.custom-nav .navbar-nav li a:before{content:'';display:none}.custom-nav .navbar-nav li a{margin:0;padding:6px 0;color:#000}.custom-nav>.container{width:90%}.custom-nav .logo .logo-dark{display:inline-block}.custom-nav .logo .logo-light{display:none}.navbar-nav{background:#8aa730;margin-top:0}.navbar-toggler{font-size:29px;margin-top:0;margin-bottom:0;color:#000}.header_title{font-size:38px;line-height:1.2}.header_subtitle{font-size:15px;line-height:1.8}.custom-nav .navbar-brand.logo img{height:26px}}@media (max-width:768px){h1{font-size:1.5rem;text-align:center}.header_title{font-size:25px}p{text-align:justify;padding:10px}.home-table-center p{text-align:center!important}.btn-outline-custom{font-size:12px}}:root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#fff;--gray:#6c757d;--gray-dark:#343a40;--primary:#007bff;--secondary:#6c757d;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}*,::after,::before{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-ms-overflow-style:scrollbar}@-ms-viewport{width:device-width}nav,section{display:block}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}h1,h2{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}ul{margin-top:0;margin-bottom:1rem}a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects}img{vertical-align:middle;border-style:none}button{border-radius:0}button{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button{overflow:visible}</style>


</head>

<body>


