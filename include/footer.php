  <footer class="footer_detail text-center">
            <div class="container">
<div class="row pb-2 pt-2">
            <div class="col-lg-4 column">
                <a href="#"> Condizioni tariffarie </a>
                </div>
                <div class="col-lg-4 column">
                    <a href="#"> Dati personali </a>
                </div>
                <div class="col-lg-4 column">
                    <a href="#"> Politica sui cookie </a>
                </div>

        </div>

                <div class="fot_bor"></div>
                <div class="row pt-3 pb-3">
                    <div class="col-lg-12">
                        <div class="float_none">
                            <p class="copy-rights mb-0">2021 &copy; Arcmovie. Tutti i diritti riservati..</p>
                        </div>
                    </div>
                </div>                
            </div>
        </footer>

      
        
        <script src="/include/js/jquery-3.1.0.min.js"></script>
        <script src="/include/js/bootstrap.min.js"></script>
<script>var cb = function() {
var l = document.createElement('link'); l.rel = 'stylesheet';
l.href = '/include/css/critical_app.css';
var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h);
};
var raf = requestAnimationFrame || mozRequestAnimationFrame ||
webkitRequestAnimationFrame || msRequestAnimationFrame;
if (raf) raf(cb);
else window.addEventListener('load', cb);</script>
      

    </body>

</html>
