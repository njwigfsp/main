<?php include('include/head.php') ?>


    <?php include('include/header.php') ?>
        <section class="home-bg1 home-clip section h-120vh" id="home">
            <div class="bg-overlay"></div>
            <div class="home-table">
                <div class="home-table-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="text-white text-center">
                                    <h1 class="header_title mb-0 mt-3 text-center text-white mx-auto">Nutrizione sportiva di Arcmovie<br>
                                    </h1>
                                    <p class="header_subtitle text-white mx-auto mt-3 mb-0"> Ogni palestra e allenatore sportivo di forza dirà che la nutrizione sportiva è necessaria per ottenere risultati più rapidi in allenamento. Oggi è impossibile ottenere risultati elevati con le proprie riserve corporee o con una cattiva qualità e un'alimentazione inutile. In realtà, la nutrizione sportiva stessa, che può essere acquistata attraverso il nostro negozio online, ed è stato progettato per fornire una composizione di sostanze utili per un rapido recupero, aumento della forza, aumento del volume muscolare e lo scarico dei depositi di grasso.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section" id="about">
            <div class="container">
                <div class="row mt-3 vertical-content">
                    <div class="col-lg-6">
                        <div class="about_content mt-3">
                            <h3 class="mb-3 font-weight-bold text-custom">Qual è il segreto della nutrizione sportiva?</h3>
                            <p class="text-muted mt-3">Associazione della nutrizione sportiva è la forma concentrata dei nutrienti che contengono. Ad esempio, anche in 100 grammi di uova, le proteine sono contenute solo fino a 15 grammi. A sua volta, l'integratore proteico contiene il 60-90% di proteine per 100 grammi di polvere. Senti la differenza. In primo luogo, la forma in cui viene fornita la proteina è rapida da digerire. L'apparato digerente non ha bisogno di grandi sforzi per lavorare il prodotto, ma solo per assimilare le proteine pronte all'uso. Lo stesso vale per l'alimentazione sportiva chiamata aminoacidi, che sono scomposti e pronti ad assimilare le proteine.</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-img mt-3">
                            <img src="/include/img/img4.jpg " alt="Nutrizione sportiva - 4" title="Nutrizione sportiva - 4" class="img-fluid mx-auto d-block position-relative about-tween">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="home-bg2" id="home">
            <div class="bg-overlay"></div>
            <div class="home-table">
                <div class="home-table-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="text-white text-center">
                                    <h3 class="header_title mb-0 mt-3 text-center text-white mx-auto pt-3">Nutrizione sportiva per la perdita di peso<br>
                                    </h3>
                                    <p class="header_subtitle textwhite text-white mx-auto mt-3 mb-0 pb-3"> Nutrizione dimagrante sportiva - questi sono bloccanti grassi o bruciatori di grasso. Contribuiscono a sopprimere l'assorbimento, l'assorbimento e la deposizione dei grassi. Ancora più importante, aiutano a liberare più velocemente l'energia dai depositi di grasso, cioè a scomporre i grassi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php include('include/footer.php') ?>  